function essay(letters) {
    // var length
    // var array = []
    // for(var i = 0; i < letters.length; i++){
    //     length = letters[i].length
    //     array[i] = length
    // }
    return letters.map(c => c.length)
}
var city = ["Bandung", "Magelang", "Surabaya", "Bekasi", "Depok"]
console.log(city.sort((a,b) => b.length - a.length))

function big(word) {
    // var countBig = 0
    // for(var i = 0; i < word.length; i++){
    //     if(word[i].length > 6) {
    //         countBig += 1
    //     }
    // }
    return word.map(c => c.length).filter(c => c > 6).length
}
console.log(big(city))